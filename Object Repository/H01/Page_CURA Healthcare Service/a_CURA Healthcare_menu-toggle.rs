<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_CURA Healthcare_menu-toggle</name>
   <tag></tag>
   <elementGuidId>75c19a14-7ddf-40a6-8c99-719781fcedaa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#menu-toggle</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='menu-toggle']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>bb15c8bb-2226-4f48-a591-d5a345be11e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>menu-toggle</value>
      <webElementGuid>596ee370-c2f7-407c-92cb-84fa6d6bb820</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>5c8c9678-c3c5-4658-aa78-3f563826f4ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-dark btn-lg toggle</value>
      <webElementGuid>0ead45dd-95ca-48bc-8b56-be9e4fa203ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;menu-toggle&quot;)</value>
      <webElementGuid>6eb75ffc-b941-419e-aa7b-36d1ac1f2862</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='menu-toggle']</value>
      <webElementGuid>4efde3f5-50a5-4380-9b08-4fcd71f08b1f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CURA Healthcare'])[1]/preceding::a[2]</value>
      <webElementGuid>7ae3d1b1-d5a4-48c0-8714-e847b183564e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/preceding::a[3]</value>
      <webElementGuid>93c78a7b-bb61-47f0-81b8-c21cebe8b32f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '#')]</value>
      <webElementGuid>fd024a1f-5d3a-4fa7-b978-1cdbcdd79c42</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a</value>
      <webElementGuid>94edd74b-543f-4063-928f-525f65ab4942</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'menu-toggle' and @href = '#']</value>
      <webElementGuid>83eaa4b7-9a56-40a7-947c-a6ccc6044708</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
